# Assignment 2 - Agile Software Practice.

Name: Wuzhen Ye

## Client UI.

![][home]

>>Shows homepage and has a link to login page

![][beverages]

>>Shows all records and allows the user to edit, remove, filter

![][edit]

>>Edits existing record, entered from manage beverages page

![][add]

>>Adds new record

![][map]

>>Shows the locations of all existing vending machines

![][contact]

>>Allows the user to add comment

![][login]

>>Allows admins to log in

![][logout1]

>>Before admins log in

![][logout2]

>>Allows admins to log out after they log in

## E2E/Cypress testing.

- [Cypress Dashboard] (https://dashboard.cypress.io/projects/3zj1ja/runs/12/specs)

## Web API CI.

(Optional) State the GitLab Pages URL of the coverage report for your Web API tests, e.g.

https://20086431.gitlab.io/assn01-cicd/coverage/lcov-report/
## GitLab CI.

(Optional) State any non-standard features (not covered in the lectures or labs) of the GitLab CI platform that you utilized.


[home]: ./screenshots/home.png
[beverages]: ./screenshots/beverages.png
[edit]: ./screenshots/edit.png
[add]: ./screenshots/add.png
[map]: ./screenshots/map.png
[contact]: ./screenshots/contact.png
[login]: ./screenshots/login.png
[logout1]: ./screenshots/logout-beforeLogin.png
[logout2]: ./screenshots/logout-afterLogin.png
